# Copyright (c) 2019, Juniper Networks, Inc.
# All rights reserved.

include .env

all: up pull

up:  id_rsa jinstall-vqfx-10-f-18.4R1.8.img junos-vmx-x86-64-18.4R1.8.qcow2
	docker-compose up -d
	/bin/bash scripts/create_links.sh

pull:
	docker-compose pull

id_rsa:
	ssh-keygen -t rsa -N "" -f id_rsa
	chmod 400 id_rsa

jinstall-vqfx-10-f-18.4R1.8.img:
	wget https://www.dropbox.com/s/p776lvt2emhrgq4/jinstall-vqfx-10-f-18.4R1.8.img

junos-vmx-x86-64-18.4R1.8.qcow2:
	wget https://www.dropbox.com/s/5wompol821zr9x7/junos-vmx-x86-64-18.4R1.8.qcow2

ps:
	docker-compose ps
	/bin/bash scripts/getpass.sh

down:
	docker-compose exec gitlab-runner gitlab-runner unregister -n ${CI_RUNNER_DESCRIPTION} -u ${CI_SERVER_URL} || true
	docker-compose down || true
	docker volume prune -f

clean:
	docker system prune -f
	docker volume prune -f
