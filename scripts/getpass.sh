#!/bin/bash
# Copyright (c) 2018, Juniper Networks, Inc.
# All rights reserved.
#
# simple script to extract root passwords from vmx log file

source .env

if [ "$USER" == "root" ]; then
  CLI=cli
fi

for device in $(docker-compose ps -q); do
  name=$(docker inspect --format='{{.Name}}' $device)
  name=$(basename $name)
  descr=$(docker logs $device 2>/dev/null | grep ' password ' )
  #descr=$(docker logs $device 2>/dev/null | grep ' root\|user password ' | cut -d\| -f2)
  if [ ! -z "$descr" ]; then
    echo $name $descr
  fi
done
exit 0
